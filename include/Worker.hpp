#ifndef WORKER_HPP
#define WORKER_HPP

#include "Board.hpp"
#include "Communication.hpp"
#include "Task.hpp"

#include <variant>

class Worker {
  private:
    static constexpr int tag   = 100;
    Board my_board;
    Task current_task;
    int depth = 6;
    bool over = false;

  public:
    Worker(int depth);

    auto evaluate_move(bool cpu_turn, int depth) -> double;
    auto do_work() -> void;
    auto is_done() -> bool;

  private:
    auto process_task(Task task) -> void;
    auto send_request() -> void;
    auto receive_task_or_wait(MPI_Status* status) -> std::variant<Task, Action>;
    auto receive_board_or_end() -> void;
    auto has_to_wait(MPI_Status status, std::variant<Task, Action> response)
        -> bool;
};

#endif
