#ifndef TASKS_HPP
#define TASKS_HPP

#include "Task.hpp"
#include "Board.hpp"

#include <optional>
#include <vector>

class Tasks {
  private:
    const Board& board;
    std::vector<Task> tasks;
    unsigned last_given = 0;
    bool active_tasks = true;

  public:
    Tasks(const Board& board);

    auto get_task() -> std::optional<Task>;
    auto get_number() const -> unsigned;
    auto get_average_score(unsigned column) -> double;
    auto empty() -> bool;
};

#endif
