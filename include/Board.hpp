#ifndef BOARD_HPP
#define BOARD_HPP

#include <array>
#include <ostream>

/* Token 'Free' must be first, so array initializer can use it by default. */
enum class Token { Free, CPU, Human };

class Board {
   private:
    constexpr static unsigned nrows = 7, ncols = 7;
    constexpr static unsigned n = 4;

    // Initializes all tokens to free.
    std::array<std::array<Token, ncols>, nrows> tokens = {};

    Token last_played = Token::Human;
    unsigned last_column, last_row;

   public:
    auto do_move(Token token, unsigned column) -> void;
    auto undo_move(unsigned column) -> void;

    auto test_victory() -> bool;
    auto is_full() -> bool;

    auto has_free_tokens(unsigned column) const -> bool;
    auto get_n_of_free_tokens(unsigned column) const -> unsigned;
    auto get_n_of_free_columns() const -> unsigned;
    auto is_move_legal(unsigned column) -> bool;

    static constexpr auto get_nrows() -> unsigned { return nrows; }
    static constexpr auto get_ncols() -> unsigned { return ncols; }

    auto friend operator<<(std::ostream& os, const Board& board)
        -> std::ostream&;

   private:
    template <typename T>
    auto test_stripe(const T& stripe) -> bool;

    auto get_column(unsigned c) const -> std::array<Token, nrows>;
    auto get_row(unsigned r) const -> std::array<Token, ncols>;
    auto get_diagonal(unsigned r, unsigned c, bool main) const
        -> std::array<Token, std::min(nrows, ncols)>;
};

template <typename T>
auto Board::test_stripe(const T& stripe) -> bool {
    unsigned count = 0;
    for (unsigned i = 0; i < stripe.size(); i++) {
        if (i > 0 && stripe[i - 1] != last_played) {
            count = 0;
        }
        if (stripe[i] == last_played) {
            count++;
        }
        if (count == n) {
            return true;
        }
    }

    return false;
}

#endif
