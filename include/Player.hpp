#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Board.hpp"

class Player {
  protected:
    Board& board;

  public:
    Player(Board& board);

    virtual auto do_move() -> void;
    virtual auto has_won() -> bool;
};

#endif
