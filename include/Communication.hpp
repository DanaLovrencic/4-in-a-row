#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <mpi.h>

enum class Action { End, Wait, TaskRequest };
enum class Tag : int { Board = 90, Action = 91, Result = 92, Task = 93,
    TaskRequest = 94, Any = MPI_ANY_TAG };

namespace Communication {

template<typename T>
auto send(const T& data, int dest, Tag tag) -> void
{
    MPI_Send(&data,
             sizeof(data),
             MPI_BYTE,
             dest,
             static_cast<int>(tag),
             MPI_COMM_WORLD);
}

template<typename T>
auto receive(int src, Tag tag, MPI_Status* status) -> T
{
    T data;
    MPI_Recv(&data,
             sizeof(data),
             MPI_BYTE,
             src,
             static_cast<int>(tag),
             MPI_COMM_WORLD,
             status);

    return data;
}

}

#endif
