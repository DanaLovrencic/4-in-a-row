#include "Computer.hpp"
#include "Worker.hpp"

#include <algorithm>
#include <limits>
#include <iomanip>

Computer::Computer(Board& board, int depth, int argc, char* argv[])
    : Player(board) // , tasks(board)
{
    int process_rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &n_workers);

    n_active_workers = --n_workers; // One process is master.

    if (process_rank != 0) {
        Worker worker(depth);
        worker.do_work();
        MPI_Finalize();
        exit(0);
    }
}

Computer::~Computer()
{
    MPI_Finalize();
}

auto Computer::do_move() -> void
{
    std::vector<double> results;
    results.resize(board.get_ncols() * board.get_ncols());
    Tasks tasks(board);

    MPI_Status status;
    n_active_workers = n_workers;

    send_board(); // Send board to all workers.

    over = false;
    while (!over) {
        // Wait for task request or result.
        auto msg = receive_message(&status);

        // Task request is received.
        if (status.MPI_TAG == static_cast<int>(Tag::TaskRequest)) {
            // auto m = std::get<Action>(msg);

            auto task = tasks.get_task();
            send_task_or_wait(status, task);
        } else if (status.MPI_TAG == static_cast<int>(Tag::Result)) {
            // Result is received.
            store_result(std::get<std::pair<Task, double>>(msg), results);
        }
    }

    // Put token on board.
    board.do_move(Token::CPU, get_best_column(results));

    // End of the game.
    notify_about_end();
}

auto Computer::has_won() -> bool
{
    return board.test_victory();
}

auto Computer::get_average_score(const std::vector<double>& results,
                                 unsigned column) -> double
{
    double score = 0;
    unsigned n   = 0;

    for (unsigned i = board.get_ncols() * column;
         i < board.get_ncols() * (column + 1);
         ++i) {
        if (results[i] == -1) { return -1; }
        if (results[i] != -std::numeric_limits<double>::infinity()) {
            score += results[i];
            ++n;
        }
    }

    return n ? score / n : -std::numeric_limits<double>::infinity();
}

auto Computer::get_best_column(const std::vector<double> results) -> unsigned
{
    std::array<double, Board::get_ncols()> scores;

    // Calculate average scores.
    for (unsigned c = 0; c < scores.size(); c++) {
        scores[c] = get_average_score(results, c);
        if (scores[c] != -std::numeric_limits<double>::infinity()) {
            std::cout << std::fixed << std::setprecision(3) << scores[c];
        } else {
            std::cout << '-';
        }

        if (c != scores.size() - 1) { std::cout << " "; }
    }
    std::cout << '\n';

    return std::distance(scores.begin(),
                         std::max_element(scores.begin(), scores.end()));
}

auto Computer::notify_about_end() -> void
{
    if (board.test_victory() || board.is_full()) {
        std::variant<Action, Board> msg = Action::End;
        for (int i = 1; i <= n_workers; ++i) {
            Communication::send(msg, i, Tag::Action);
        }
    }
}

auto Computer::send_board() -> void
{
    std::variant<Action, Board> msg = board;
    for (int i = 1; i <= n_active_workers; ++i) {
        Communication::send(msg, i, Tag::Board);
    }
}

auto Computer::receive_message(MPI_Status* status)
    -> std::variant<Action, std::pair<Task, double>>
{
    auto msg =
        Communication::receive<std::variant<Action, std::pair<Task, double>>>(
            MPI_ANY_SOURCE, Tag::Any, status);

    return msg;
}

auto Computer::send_task_or_wait(MPI_Status status,
                                 const std::optional<Task>& task) -> void
{
    std::variant<Task, Action> response;

    if (task) { // Send task.
        response = *task;
        Communication::send(response, status.MPI_SOURCE, Tag::Task);
    } else { // No available tasks.
        response = Action::Wait;
        if (--n_active_workers == 0) { over = true; }
        Communication::send(response, status.MPI_SOURCE, Tag::Action);
    }
}

auto Computer::store_result(const std::pair<Task, double>& result,
                            std::vector<double>& results) -> void
{
    Task task                     = result.first;
    results[task.get_computer_col() * board.get_ncols() +
            task.get_human_col()] = result.second;
}
