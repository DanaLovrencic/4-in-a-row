#include "Player.hpp"

#include <iostream>

Player::Player(Board& board) : board(board) {}

auto Player::do_move() -> void
{
    unsigned human_col;
    do {
        std::cin >> human_col;
    } while (!board.is_move_legal(human_col));
    board.do_move(Token::Human, human_col);
}

auto Player::has_won() -> bool
{
    return board.test_victory();
}
