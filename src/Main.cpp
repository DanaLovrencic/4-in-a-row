#include "Computer.hpp"
#include "Player.hpp"

#include <iostream>
#include <chrono>

int main(int argc, char* argv[])
{
    Board board;
    Player player(board);
    Computer computer(board, 6, argc, argv); // Depth is 8.

    while (!board.is_full()) {
        player.do_move();
        if (player.has_won()) { break; }

        computer.do_move();
        if (computer.has_won()) { break; }

        std::cout << board << std::endl;
    }
    std::cout << board << std::endl;
}
